var mweFormsClient = {
  send: function send(formId, formData) {
    var apiUrl = 'https://formsapi.medicalwebexperts.com/api/v1/emails/send';

    formData.formId = formId;

    return new Promise(function(resolve, reject) {
      var xmlhttp = new XMLHttpRequest();

      xmlhttp.responseType = 'json';

      xmlhttp.open('POST', apiUrl, true);
      xmlhttp.setRequestHeader('Content-Type', 'application/json');
      xmlhttp.onreadystatechange = function() {
        if (xmlhttp.status === 200) {
          resolve({
            success: true,
            message:
              'Thank you for contacting us. We will be in touch with you shortly',
          });
        } else {
          reject({
            success: false,
            message:
              'An error occurred while submitting your contact form. Please reload the page and try your submission again',
          });
        }
      };

      xmlhttp.onerror = function(err) {
        reject({
          success: false,
          message:
            'An error occurred while submitting your contact form. Please reload the page and try your submission again',
        });
      };

      xmlhttp.send(JSON.stringify(formData));
    });
  },
};
